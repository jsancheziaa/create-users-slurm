# create-users-slurm

## General Information

This is an ansible playbook to create users in slurm cluster


## Requirements

Ansible installed.
Change the inventory file, adding the nodes improved
Change the user.yml, add or modify usernames, uids, groups


## Run it
`sudo ansible-playbook create_users.yml -e @users.yml -i inventory`
</details>

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
